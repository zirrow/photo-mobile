<?php

// Heading
$_['heading_title'] = 'AlphaSms.ua';
$_['text_module']   = 'Modules';
$_['text_edit']   = 'Edit module AlphaSms.ua';

$_['alphasms_saved_success'] = 'Success saved settings';
$_['alphasms_smssend_success'] = 'SMS successfully sent to gateway';

// Error
$_['alphasms_error_permission'] = 'You have not authority to change settings of this module!';
$_['alphasms_error_request'] = 'Request failed';
$_['alphasms_error_auth_info'] = 'You must set authorize setting to SMS-Gate';
$_['alphasms_error_login_field'] = 'You must specify a login';
$_['alphasms_error_password_field'] = 'You must specify a password';
$_['alphasms_error_sign_field'] = 'You must specify a signature';
$_['alphasms_error_admphone_field'] = 'You must set admin phone';
$_['alphasms_error_sign_to_large'] = 'Signature is to large. Maximum 11 symbols';
$_['alphasms_error_empty_frmsms_message'] = 'You must specify a text of message';
$_['alphasms_error_frmsms'] = 'Error with sending a message';

// Tabs name in view
$_['alphasms_tab_connection'] = 'Gatawey settings';
$_['alphasms_tab_signature'] = 'Signature';
$_['alphasms_tab_events'] = 'Execute on events';
$_['alphasms_tab_about'] = 'About';
$_['alphasms_tab_sendsms'] = 'Send SMS';

// Text messges
$_['alphasms_text_gate_settings'] = 'Gate settings';
$_['alphasms_text_login'] = 'Login';
$_['alphasms_text_login_placeholder'] = 'AlphaSms.ua login (phone)';
$_['alphasms_error_login'] = 'Empty phone or wrong format. Must be e.g. +380112223344';
$_['alphasms_text_password'] = 'Password';
$_['alphasms_error_password'] = 'Password must be not empty !';
$_['alphasms_text_key'] = 'API Key';
$_['alphasms_error_key'] = 'Empty API Key';
$_['alphasms_text_sign'] = 'Signature';
$_['alphasms_error_sign'] = 'Empty signature or wrong format. Must be no longer 11 chars !';
$_['alphasms_text_admphone'] = 'Admin phone';
$_['alphasms_error_admphone'] = 'Empty administrator phone or wrong format. Must be e.g. +380112223344';
$_['alphasms_text_phone'] = 'Recipient phone';
$_['alphasms_error_phone'] = 'Empty phone or wrong format. Must be e.g. +380112223344';
$_['alphasms_text_notify_sms_to_admin'] = 'Notify evens to admin';
$_['alphasms_text_notify_sms_to_customer'] = 'Notify evens to customer';
$_['alphasms_text_connection_established'] = 'Connection to gateway is established';
$_['alphasms_text_connection_error'] = 'Gateway is not connected';
$_['alphasms_events_admin_new_customer'] = 'The new customer is registered';
$_['alphasms_events_admin_new_order'] = 'A new order is implemented';
$_['alphasms_events_admin_new_email'] = 'Received new email with store contact form';
$_['alphasms_text_frmsms_message'] = 'Text of message';
$_['alphasms_error_message'] = 'Empty message';
$_['alphasms_text_frmsms_phone'] = 'Destination phone';
$_['alphasms_text_button_send_sms'] = 'Send SMS';
$_['alphasms_events_admin_gateway_connection_error'] = 'Notify me by email at the gateway connection fails';
$_['alphasms_events_customer_new_order_status'] = 'Change the order status';
$_['alphasms_events_customer_new_order'] = 'The buyer a message about a new order';
$_['alphasms_events_customer_new_register'] = 'The registration is completed successfully';

$_['alphasms_message_customer_new_order_status'] = 'Order status #{order_id} is changed on "{new_status_name}".';

$_['alphasms_text_connection_tab_description'] =
'Enter the correct information to connect to gateway AlphaSms.ua via HTTP/HTTPS protocol.<br/>';

$_['alphasms_text_about_tab_description'] =
'<b>%s &copy; %s All rights reserved</b><br />
<br/>
Module is designed to send SMS notifications via gateway AlphaSms.ua.
<br/><br/>
This product is distributed under a BSD License<br/><br/>
Current version: %s<br />';

// UPD from 2016-08-04

$_['alphasms_tab_templates'] = 'Messages templates';
$_['alphasms_connection_error_title'] = 'Error with gateway connection';
$_['alphasms_customer_new_register_title'] = 'Congratulations to the user registration';
$_['alphasms_customer_new_order_title'] = 'Post purchase';
$_['alphasms_admin_new_customer_title'] = 'Notification to admin about new user';
$_['alphasms_admin_new_order_title'] = 'Notification to admin about new order';
$_['alphasms_admin_new_email_title'] = 'Notification to admin about message to support';
$_['alphasms_customer_new_order_status_title'] = 'Notice of change of order status';
$_['alphasms_text_button_save_templates'] = 'To save messages templates';


// Messages templates variables:

$_['alphasms_variables_notice'] = 'You can not fill out all the fields in the form, and then some of the variables in the SMS will be replaced with empty values';
$_['alphasms_variable_shop_name'] = 'Store Name';
$_['alphasms_variable_customer_id'] = 'user number';
$_['alphasms_variable_firstname'] = 'Name';
$_['alphasms_variable_lastname'] = 'Name';
$_['alphasms_variable_email'] = 'Email';
$_['alphasms_variable_telephone'] = 'Telephone';
$_['alphasms_variable_fax'] = 'Fax';
$_['alphasms_variable_company'] = 'Company';
$_['alphasms_variable_address_1'] = 'Address 1';
$_['alphasms_variable_address_2'] = 'Address 2';
$_['alphasms_variable_city'] = 'City';
$_['alphasms_variable_postcode'] = 'Post Code';
$_['alphasms_variable_password'] = 'Password';
$_['alphasms_variable_ip'] = 'IP';
$_['alphasms_variable_order_id'] = 'Order number';
$_['alphasms_variable_store_url'] = 'URL store';
$_['alphasms_variable_custom_field'] = 'Custom Field';
$_['alphasms_variable_payment_method'] = 'Payment method';
$_['alphasms_variable_shipping_address_1'] = 'Shipping Address 1';
$_['alphasms_variable_shipping_address_2'] = 'Shipping Address 2';
$_['alphasms_variable_shipping_postcode'] = 'mail service code';
$_['alphasms_variable_shipping_city'] = 'City of delivery';
$_['alphasms_variable_shipping_region'] = 'Region / Delivery Area';
$_['alphasms_variable_shipping_country'] = 'Delivery Country';
$_['alphasms_variable_shipping_method'] = 'Delivery method';
$_['alphasms_variable_comment'] = 'Comment';
$_['alphasms_variable_total'] = 'Sum';
$_['alphasms_variable_currency_code'] = 'Currency code';
$_['alphasms_variable_date_added'] = 'Date';
$_['alphasms_variable_name'] = 'Name';
$_['alphasms_variable_enquiry'] = 'text query / question in the Support';
$_['alphasms_variable_new_status_name'] = 'The name of the new status';
$_['alphasms_variable_date_modified'] = 'Date Modified';
$_['alphasms_variable_products_links'] = 'All order products links';

/**
 * Viber
 */
$_['alphasms_sending_to_Viber'] = 'Sending messages to Viber';
$_['alphasms_viber_only_standart_sms'] = 'Standard SMS only (do not use Viber)';
$_['alphasms_viber_sms_if_inactive'] = 'Send to Viber if the recipient is active, otherwise - SMS';
$_['alphasms_viber_only_viber'] = 'Send to Viber only';
$_['alphasms_text_viber_sign'] = 'The sender\'s name for Viber';
$_['alphasms_error_viber_sign_field'] = 'Error in sender\'s name';
