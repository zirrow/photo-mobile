<?php
// Heading
$_['heading_title']		= 'Quick Edit';

$_['text_module'] 		= 'Modules';
$_['text_status']		= 'Status';
$_['text_success']		= 'Success: You have successfully modified Quick Edit';
$_['text_enabled']		= 'Enabled';
$_['text_disabled']		= 'Disabled';


$_['error_quickedit_status'] = 'Error: Status is required';

// Button
$_['button_save']		= 'Save';
$_['button_cancel']		= 'Cancel';

// Error
$_['error_permission']	= 'Warning: You do not have permission to modify Quick Edit!';