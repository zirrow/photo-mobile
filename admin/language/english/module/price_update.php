<?php
// Heading
$_[ 'heading_title' ] = 'Price Update and products quantities';

// Text
$_[ 'entry_name' ]    = 'Price Update and products quantities';
$_[ 'text_edit' ]         = 'Working with price update module';
$_[ 'save_db_structure' ] = 'Save current DB structure';
$_[ 'download_price' ]    = 'Download price';
$_[ 'upload_new_price' ]  = 'Upload new price';
$_[ 'update_price_subtit_text' ]  = 'Update price';
$_[ 'success_upadte' ]  = 'The price was updated successfully';
$_[ 'fail_upadte' ]  = 'The price was not updated';
$_[ 'not_correct_file_type' ]  = 'The file type must be xls or xlsx';