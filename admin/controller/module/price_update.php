<?php

class ControllerModulePriceUpdate extends Controller
{
    private $error = array();

    public function index()
    {
        // Load module language file
        $this->load->language( 'module/price_update' );

        // Load price update style sheet
        $this->document->addStyle( 'view/stylesheet/price_update.css' );

        // Add a title of the page
        $this->document->setTitle( $this->language->get( 'heading_title' ) );

        // Loading models
        $this->load->model( 'extension/module' );
        $this->load->model( 'extension/price_update' );

        // If button Save DB structure was pushed - saving DB structure
        if ( isset( $_REQUEST[ 'excel_submit_button' ] ) ) {
            $this->model_extension_price_update->get_db_structure();
        }

        // If button excel_input_file was pushed - saving DB structure
        if ( ( isset( $_FILES[ 'excel_input_file' ] ) ) && ( isset( $_REQUEST[ 'update_price_submit_button' ] ) ) ) {

            // Check if the file has excel extensions
            $file_name_ext = $_FILES[ 'excel_input_file' ][ 'name' ];
            $file_ext      = explode( '.', $file_name_ext );
            $file_ext      = $file_ext[ 1 ];

            // Class for message block
            $data[ 'class_message' ] = '';

            if ( 'xls' == $file_ext || 'xlsx' == $file_ext ) {
                $returned_value = $this->model_extension_price_update->upload_new_price_list( $_FILES[ 'excel_input_file' ][ 'tmp_name' ] );
                if ( $returned_value ) {
                    $data[ 'result_update' ] = $this->language->get( 'success_upadte' );
                    $data[ 'class_message' ] = 'alert-success';
                } else {
                    $data[ 'result_update' ] = $this->language->get( 'fail_upadte' );
                    $data[ 'class_message' ] = 'alert-danger';
                }
            } else {
                $data[ 'result_update' ] = $this->language->get( 'not_correct_file_type' );
                $data[ 'class_message' ] = 'alert-danger';
            }

        }

        // Language variables
        $data[ 'heading_title' ]            = $this->language->get( 'heading_title' );
        $data[ 'entry_name' ]               = $this->language->get( 'entry_name' );
        $data[ 'text_edit' ]                = $this->language->get( 'text_edit' );
        $data[ 'save_db_structure' ]        = $this->language->get( 'save_db_structure' );
        $data[ 'download_price' ]           = $this->language->get( 'download_price' );
        $data[ 'upload_new_price' ]         = $this->language->get( 'upload_new_price' );
        $data[ 'update_price_subtit_text' ] = $this->language->get( 'update_price_subtit_text' );

        if ( isset( $this->error[ 'warning' ] ) ) {
            $data[ 'error_warning' ] = $this->error[ 'warning' ];
        } else {
            $data[ 'error_warning' ] = '';
        }

        $url = '';

        if ( isset( $this->request->get[ 'sort' ] ) ) {
            $url .= '&sort=' . $this->request->get[ 'sort' ];
        }

        if ( isset( $this->request->get[ 'order' ] ) ) {
            $url .= '&order=' . $this->request->get[ 'order' ];
        }

        if ( isset( $this->request->get[ 'page' ] ) ) {
            $url .= '&page=' . $this->request->get[ 'page' ];
        }

        // Set breadcrumbs for admin module page
        $data[ 'breadcrumbs' ]   = array();
        $data[ 'breadcrumbs' ][] = array(
            'text' => $this->language->get( 'text_home' ),
            'href' => $this->url->link( 'common/dashboard', 'token=' . $this->session->data[ 'token' ], 'SSL' )
        );

        $data[ 'breadcrumbs' ][] = array(
            'text' => $this->language->get( 'heading_title' ),
            'href' => $this->url->link( 'module/price_update', 'token=' . $this->session->data[ 'token' ] . $url, 'SSL' )
        );

        // Set action URL for form
        $data[ 'action' ] = $this->url->link( 'module/price_update', 'token=' . $this->session->data[ 'token' ], 'SSL' );
        $data[ 'header' ]      = $this->load->controller( 'common/header' );
        $data[ 'column_left' ] = $this->load->controller( 'common/column_left' );
        $data[ 'footer' ]      = $this->load->controller( 'common/footer' );

        $this->response->setOutput( $this->load->view( 'module/price_update.tpl', $data ) );

    }

}