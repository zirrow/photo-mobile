<?php
class ControllerModuleQuickedit extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/quickedit');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('quickedit', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$languageStrings = array('heading_title', 'text_module', 'text_success', 'error_permission', 'text_status', 'text_enabled', 'text_disabled', 'button_save', 'button_cancel');
		foreach ($languageStrings as $languageString) {
			$data[$languageString] = $this->language->get($languageString);
		}

		$requiredFields = array('quickedit_status');
		foreach ($requiredFields as $requiredField) {
			if (isset($this->error[$requiredField])) {
				$data['error_'.$requiredField] = $this->error[$requiredField];
			} else {
				$data['error_'.$requiredField] = '';
			}
		}

		$settings = $this->model_setting_setting->getSetting('quickedit');

		$fields = array('quickedit_status');
		foreach ($fields as $field) {
			if (isset($this->request->post[$field])) {
				$data[$field] = $this->request->post[$field];
			} elseif (isset($settings[$field])) {
				$data[$field] = $settings[$field];
			} else {
				$data[$field] = '';
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/quickedit', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/quickedit', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/quickedit.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/quickedit')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$requiredFields = array('quickedit_status');
		foreach ($requiredFields as $requiredField) {
			if (!isset($this->request->post[$requiredField])) {
				$this->error[$requiredField] = $this->language->get('error_'.$requiredField);
			}
		}

		return !$this->error;
	}

	public function install() {
		$this->load->model('setting/setting');

		$this->model_setting_setting->editSetting('quickedit', array('quickedit_status' => 1));

		$this->load->model('extension/modification');

		$modification = array();
		$modification['name'] = 'QuickEdit';
		$modification['author'] = 'Quin Solutions';
		$modification['version'] = '1.0';
		$modification['link'] = 'http://quinsolutions.net';
		$modification['status'] = 1;

		if (VERSION == '2.0.0.0') {
			$modification['code'] = '<?xml version="1.0" encoding="UTF-8"?>
				<modification>
					<name>QuickEdit</name>
					<version>1.0</version>
					<author>Quin Solutions</author>
					<link>http://quinsolutions.net</link>
					<file path="catalog/controller/common/header.php">
						<operation>
							<search><![CDATA[
							$data[\'cart\'] = $this->load->controller(\'common/cart\');
							]]></search>
							<add position="after"><![CDATA[
							$user = new User($this->registry);
							if ($this->config->get("quickedit_status") && $user->isLogged() && isset($this->request->get["route"])) {
								$admin_url = HTTPS_SERVER . "admin/index.php?token=" . $this->session->data["token"];
								if ($this->request->get["route"] == "product/product") {
									$data["quickedit_href"] = $admin_url . "&route=catalog/product/edit&product_id=" . $this->request->get["product_id"];
								} else if ($this->request->get["route"] == "product/category") {
									$path_parts = explode("_", $this->request->get["path"]);
									$data["quickedit_href"] = $admin_url . "&route=catalog/category/edit&category_id=" . array_pop($path_parts);
								} else if ($this->request->get["route"] == "information/information") {
									$data["quickedit_href"] = $admin_url . "&route=catalog/information/edit&information_id=" . $this->request->get["information_id"];
								} else if ($this->request->get["route"] == "product/manufacturer/info") {
									$data["quickedit_href"] = $admin_url . "&route=catalog/manufacturer/edit&manufacturer_id=" . $this->request->get["manufacturer_id"];
								}
							}
							]]></add>
						</operation>
					</file>
					<file path="catalog/view/theme/*/template/common/header.tpl">
						<operation>
							<search regex="true"><![CDATA[~(<body.*>)~]]></search>
							<add><![CDATA[
							$1
							<?php if (isset($quickedit_href)): ?>
							<div id="quickedit">
							  <style type="text/css" scoped>
							    #quickedit {
							      position: fixed;
							      top: 0;
							      right: 0;
							      opacity: 0.7;
							    }
							    #quickedit a {
							      background-color: #6cf;
							      padding: 8px 16px;
							      display: block;
							      font-size: 14px;
							      color: #000;
							      border-bottom-left-radius: 5px;
							    }
							  </style>
							  <a href="<?php echo $quickedit_href; ?>">EDIT</a>
							</div>
							<?php endif; ?>
							]]></add>
						</operation>
					</file>
				</modification>';
		} else {
			$modification['code'] = 'quickedit';
			$modification['xml'] = '<?xml version="1.0" encoding="UTF-8"?>
				<modification>
					<name>QuickEdit</name>
					<version>1.0</version>
					<code>quickedit</code>
					<author>Quin Solutions</author>
					<link>http://quinsolutions.net</link>
					<file path="catalog/controller/common/header.php">
						<operation>
							<search><![CDATA[
							$data[\'cart\'] = $this->load->controller(\'common/cart\');
							]]></search>
							<add position="after"><![CDATA[
							$user = new User($this->registry);
							if ($this->config->get("quickedit_status") && $user->isLogged() && isset($this->request->get["route"])) {
								$admin_url = HTTPS_SERVER . "admin/index.php?token=" . $this->session->data["token"];
								if ($this->request->get["route"] == "product/product") {
									$data["quickedit_href"] = $admin_url . "&route=catalog/product/edit&product_id=" . $this->request->get["product_id"];
								} else if ($this->request->get["route"] == "product/category") {
									$path_parts = explode("_", $this->request->get["path"]);
									$data["quickedit_href"] = $admin_url . "&route=catalog/category/edit&category_id=" . array_pop($path_parts);
								} else if ($this->request->get["route"] == "information/information") {
									$data["quickedit_href"] = $admin_url . "&route=catalog/information/edit&information_id=" . $this->request->get["information_id"];
								} else if ($this->request->get["route"] == "product/manufacturer/info") {
									$data["quickedit_href"] = $admin_url . "&route=catalog/manufacturer/edit&manufacturer_id=" . $this->request->get["manufacturer_id"];
								}
							}
							]]></add>
						</operation>
					</file>
					<file path="catalog/view/theme/*/template/common/header.tpl">
						<operation>
							<search regex="true"><![CDATA[~(<body.*>)~]]></search>
							<add><![CDATA[
							$1
							<?php if (isset($quickedit_href)): ?>
							<div id="quickedit">
							  <style type="text/css" scoped>
							    #quickedit {
							      position: fixed;
							      top: 0;
							      right: 0;
							      opacity: 0.7;
							    }
							    #quickedit a {
							      background-color: #6cf;
							      padding: 8px 16px;
							      display: block;
							      font-size: 14px;
							      color: #000;
							      border-bottom-left-radius: 5px;
							    }
							  </style>
							  <a href="<?php echo $quickedit_href; ?>">EDIT</a>
							</div>
							<?php endif; ?>
							]]></add>
						</operation>
					</file>
				</modification>';
		}

		$this->model_extension_modification->addModification($modification);
		$this->load->controller('extension/modification/refresh');
	}

	public function uninstall() {
		$this->load->model('extension/modification');
		$mods = $this->model_extension_modification->getModifications();
		foreach ($mods as $mod) {
			if ($mod['name'] == 'QuickEdit' && $mod['author'] == 'Quin Solutions') {
				$this->model_extension_modification->deleteModification($mod['modification_id']);
			}
		}
		
		$this->load->controller('extension/modification/refresh');
	}
}