<?php
class ControllerSaleCallback extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('sale/callback');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/callback');

		$this->getList();
	}

	public function getList() {

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('sale/callback', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['requests'] = array();

		$filter_data = array(
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$order_total = $this->model_sale_callback->getTotalCallbacks();

		$results = $this->model_sale_callback->getCallbacks($filter_data);

		foreach ($results as $result) {

			$date = date_create($result['date']);

			$data['requests'][] = array(
				'id'      => $result['id'],
				'tel'     => preg_replace('~\s+~s', '', preg_replace("/[^0-9\s]/","",$result['tel'])),
				'teltext' => $result['tel'],
				'name'    => $result['name'],
				'text'    => $result['text'],
				'link'    => $result['link'],
				'date'    => date_format($date, 'd-m-Y'),
				'status'  => $result['status']
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_tel'] = $this->language->get('text_tel');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_text'] = $this->language->get('text_text');
		$data['text_page'] = $this->language->get('text_page');
		$data['text_link'] = $this->language->get('text_link');
		$data['text_date'] = $this->language->get('text_date');
		$data['text_status'] = $this->language->get('text_status');
		$data['text_off'] = $this->language->get('text_off');
		$data['text_on'] = $this->language->get('text_on');

		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['token'] = $this->session->data['token'];

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

		$data['store'] = HTTPS_CATALOG;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('sale/callback_form.tpl', $data));

	}

	public function updateStatus(){

		$json = '';

		if($_POST['id']){

			$this->load->model('sale/callback');

			$status = $this->model_sale_callback->updateStatus($_POST['id']);

			$json['success'] = $status;

		} else {
			$json['error'] = 'Id is missing';
		}

		$this->response->setOutput(json_encode($json));

	}

}
