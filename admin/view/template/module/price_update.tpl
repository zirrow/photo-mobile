<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-example" name="save_button" data-toggle="tooltip"
                        title="<?php echo $button_save; ?>"
                        class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                   class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ( false == empty( $class_message ) ) { ?>
        <div class="alert <?php echo $class_message; ?>">
            <i class="fa fa-check-circle"></i>
            <?php if ( false == empty( $result_update ) ) { echo $result_update; } ?>
            <button type="button" class="close" data-dismiss="alert">×</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-simple"
                      class="form-horizontal">
                    <table class="simple-little-table" cellspacing='0' align="center">
                        <tr>
                            <th><?php echo $download_price; ?></th>
                            <th><?php echo $upload_new_price; ?></th>
                        </tr>
                        <tr>
                            <td><input type="submit" class="btn" name="excel_submit_button"
                                       value="<?php echo $save_db_structure; ?>"></td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <input type="file" name="excel_input_file">
                                        </td>
                                        <td>
                                            <input type="submit" class="btn" name="update_price_submit_button"
                                                   value="<?php echo $update_price_subtit_text; ?>">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>