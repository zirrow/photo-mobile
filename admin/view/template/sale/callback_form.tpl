<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
						<tr>
							<td class="text-center"><?php echo $text_tel; ?></td>
							<td class="text-center"><?php echo $text_name; ?></td>
							<td class="text-center"><?php echo $text_text; ?></td>
							<td class="text-center"><?php echo $text_page; ?></td>
							<td class="text-center"><?php echo $text_date; ?></td>
							<td class="text-center"><?php echo $text_status; ?></td>
						</tr>
						</thead>
						<tbody>
						<?php if ($requests) { ?>
							<?php foreach ($requests as $request) { ?>
							<tr>
								<td class="text-center"><a href="tel:<?php echo $request['tel']; ?>"><?php echo $request['teltext']; ?></a></td>
								<td class="text-center"><?php echo $request['name']; ?></td>
								<td class="text-center"><?php echo $request['text']; ?></td>
								<td class="text-center"><a href="<?php echo $request['link']; ?>"><?php echo $text_link; ?></a></td>
								<td class="text-center"><?php echo $request['date']; ?></td>
								<td class="text-center">
									<?php if($request['status'] == '0') { ?>
										<span class="off status" data-id="<?php echo $request['id']; ?>"><?php echo $text_off ?></span>
									<?php } else { ?>
										<span class="on status" data-id="<?php echo $request['id']; ?>"><?php echo $text_on ?></span>
									<?php } ?>
								</td>
							</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>

<script type="text/javascript"><!--
$('.status').on('click',function () {

    var self = $(this);

    $.ajax({
        url: 'index.php?route=sale/callback/updateStatus&token=<?php echo $token; ?>',
	    type: 'POST',
	    data: {id:self.data('id')},
        dataType: 'json',
        success: function(json) {
            if (json.success == '1'){
                self.removeClass('off').addClass('on').html('<?php echo $text_on ?>');
            } else {
                self.removeClass('on').addClass('off').html('<?php echo $text_off ?>');
            }
        }
    });

});
//--></script>