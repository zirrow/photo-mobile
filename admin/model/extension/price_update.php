<?php
$file_path = __FILE__;
$file_path = str_replace( "admin/model/extension/price_update.php", '', $file_path );
define( 'ABS_PATH_UPDATER', $file_path );
require_once( ABS_PATH_UPDATER . '/system/PHPExcel/Classes/PHPExcel.php' );
require_once( ABS_PATH_UPDATER . '/system/PHPExcel/Classes/PHPExcel/IOFactory.php' );


class ModelExtensionPriceUpdate extends Model
{

    public function index()
    {

    }


    // Get DB structure
    public function get_db_structure()
    {
        // Get structure of price table
        $db_prefix = DB_PREFIX;
        $query     = $this->db->query( "SELECT DISTINCT {$db_prefix}product.model, {$db_prefix}product.price
                                    FROM {$db_prefix}product
                                    ORDER BY {$db_prefix}product.model ASC"
        );

        // All products
        $data_arr = $query->rows;

        // Craete PHPExcel object
        $xls = new PHPExcel();

        // Set the index of the active shet
        $xls->setActiveSheetIndex( 0 );

        // Get the active sheet
        $sheet = $xls->getActiveSheet();

        // Set the name of the active sheet
        $sheet->setTitle( 'Price Update' );

        // Input names in cells
        $sheet->setCellValue( "A1", 'model' );
        $sheet->setCellValue( "B1", 'price' );

        // Align of the text
        $sheet->getStyle( 'A1' )->getAlignment()->setHorizontal(
            PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
        $sheet->getStyle( 'B1' )->getAlignment()->setHorizontal(
            PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
        $col_names = array(
            'model',
            'price',
        );

        for ( $i = 2; $i <= $query->num_rows + 1; $i++ ) {
            for ( $j = 2; $j < 4; $j++ ) {
                // Create downloaded price list
                $sheet->setCellValueByColumnAndRow(
                    $j - 2,
                    $i,
                    $data_arr[ $i - 2 ][ $col_names[ $j - 2 ] ] );
                // Apply alignment
                $sheet->getStyleByColumnAndRow( $j - 2, $i )->getAlignment()->
                setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_LEFT );
            }
        }

        // Set the  HTTP-headers
        header( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header( "Last-Modified: " . gmdate( "D,d M YH:i:s" ) . " GMT" );
        header( "Cache-Control: no-cache, must-revalidate" );
        header( "Pragma: no-cache" );
        header( "Content-type: application/vnd.ms-excel" );
        header( "Content-Disposition: attachment; filename=update_prices.xls" );

        // Downloading price list
        $objWriter = new PHPExcel_Writer_Excel5( $xls );
        $objWriter->save( 'php://output' );

    }


    // Uploading new price list
    public function upload_new_price_list( $path_of_new_price )
    {
        try {
            // Loading IO excel library object
            $xls = PHPExcel_IOFactory::load( $path_of_new_price );

            // Set active sheet
            $xls->setActiveSheetIndex( 0 );

            $row_counter = $xls->setActiveSheetIndex( 0 )->getHighestRow();
            $col_counter = $xls->setActiveSheetIndex( 0 )->getHighestColumn();

            if ( $row_counter > 1 && 'B' == $col_counter ) {
                // Working with data by rows
                for ( $i = 2; $i <= $row_counter; $i++ ) {

                    $model = $xls->getActiveSheet()->getCell( 'A' . $i )->getValue();
                    $price = $xls->getActiveSheet()->getCell( 'B' . $i )->getValue();

                    $db_prefix = DB_PREFIX;
                    if ( $this->db->query( "UPDATE {$db_prefix}product SET {$db_prefix}product.model = {$model}, {$db_prefix}product.price = {$price} WHERE {$db_prefix}product.model = {$model}" ) ) {
                        $is_true_update = true;
                    } else {
                        $is_true_update = false;
                    }
                }
            } else {
                $is_true_update = false;
            }

        } catch ( Exception $e ) {
            echo 'Excel file was not loaded: ', $e->getMessage(), "\n";
        }

        return $is_true_update;
    }

    
}