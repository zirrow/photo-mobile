<?php
class ModelSaleCallback extends Model {

	public function getTotalCallbacks() {

		$sql = "SELECT COUNT(*) AS count FROM " . DB_PREFIX . "callback";

		$query = $this->db->query($sql);

		return $query->row['count'];
	}

	public function getCallbacks($data) {

		$sql = "SELECT * FROM " . DB_PREFIX . "callback WHERE 1";

		if (isset($data['start'],$data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function updateStatus($id){

		$sql = "SELECT `status` AS status FROM " . DB_PREFIX . "callback WHERE id =".$id;

		$query = $this->db->query($sql);

		$query->row['status'];

		if ($query->row['status'] == 0){
			$sql = "UPDATE " . DB_PREFIX . "callback SET `status` = '1' WHERE id =".$id;
			$result = 1;
		} else {
			$sql = "UPDATE " . DB_PREFIX . "callback SET `status` = '0' WHERE id =".$id;
			$result = 0;
		}

		$this->db->query($sql);

		return $result;
	}

}
