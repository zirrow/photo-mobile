<?php
abstract class Controller {
	protected $registry;

    protected function alphasms_init(){

        # Load language
        $this->load->language('module/alphasms');

        $this->registry->set('alphasms_logger', new Log('alphasms.log'));

        if ((($this->config->get('alphasms_login') &&
                    $this->config->get('alphasms_password'))||$this->config->get('alphasms_key')) &&
            file_exists(DIR_SYSTEM . 'library/alphasms_gateway.php')){

            # Load AlphaSms library
            require_once(DIR_SYSTEM . 'library/alphasms_gateway.php');

            $gateway = new AlphaSmsGateway(
                $this->config->get('alphasms_login'),
                $this->config->get('alphasms_password'),
                $this->config->get('alphasms_key')
            );

            # Set sign
            $alphasms_sign = $this->config->get('alphasms_sign');
            $gateway->setSign($alphasms_sign);
            $gateway->setViber($this->config->get('alphasms_viber'));
            $gateway->setViberSign($this->config->get('alphasms_viber_sign'));

            # Add to global registry
            $this->registry->set('alphasms_gateway', $gateway);
            return true;
        }
    }

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}