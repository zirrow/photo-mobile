<?php

$_['alphasms_saved_success'] = 'Налашутвання успішно збережено';

$_['alphasms_message_connection_error'] = 'Виникли неполадки зі шлюзом AlphaSms під час пересилання SMS повідомлення.<br />Час відправки: %s<br />Відповідь серверу: %s';
$_['alphasms_message_customer_new_register'] = 'Вітаємо з успішною реєстрацією в Інтернет-магазині "{shop_name}"';
$_['alphasms_message_customer_new_order'] = 'Дякуюємо за покупку. Ваш номер замовлення #{order_id}';
$_['alphasms_message_admin_new_customer'] = 'Зареєструвався новий покупець #{customer_id} {firstname} {lastname}';
$_['alphasms_message_admin_new_order'] = 'Нове замовлення #{order_id}';
$_['alphasms_message_admin_new_email'] = 'В сапорт надіслано лита від {email}';

$_['alphasms_page_head_title'] = 'Модуль AlphaSMS';

$_['alphasms_message_customer_new_order_status'] = 'Статус замовлення #{order_id} змінився на "{new_status_name}".';
