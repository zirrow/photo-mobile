<?php
// Text
$_['text_title']				= '<img src="/image/sprite/bank.png"> Банковский перевод на карту Приватбанка (доставка 35 грн)';
$_['text_instruction']			= 'Инструкции по банковскому переводу';
$_['text_description']			= 'Пожалуйста, переведите оплату на следующий банковский счет.';
$_['text_payment']				= 'Ваш заказ не будет отправлен до получения магазином оплаты.';