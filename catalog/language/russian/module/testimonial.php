<?php
// Text
$_['heading_title']	           = 'Отзывы';

$_['text_write']               = 'Написать отзыв';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'There are no reviews.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Thank you for your review. It has been submitted to the webmaster for approval.';

$_['text_mail_subject']        = 'You have a new testimonial (%s).';
$_['text_mail_waiting']	       = 'You have a new testimonial waiting.';
$_['text_mail_author']	       = 'Author: %s';
$_['text_mail_rating']	       = 'Rating: %s';
$_['text_mail_text']	       = 'Text:';

// Entry
$_['entry_name']               = 'Your Name';
$_['entry_review']             = 'Your Review';
$_['entry_rating']             = 'Rating';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

// Button
$_['button_continue']          = 'Продолжить';

// Error
$_['error_name']               = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']               = 'Warning: Review Text must be between 25 and 3000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';
$_['error_captcha']            = 'Warning: Verification code does not match the image!';


// Entry
$_['entry_qty']                               = 'Кол-во';
$_['entry_name']                              = 'Ваше имя:';
$_['entry_review']                            = 'Ваш отзыв';
$_['entry_rating']                            = 'Рейтинг';
$_['entry_good']                              = 'Хорошо';
$_['entry_bad']                               = 'Плохо';
$_['entry_captcha']                           = 'Введите защитный код в поле ниже';

$_['text_reviews']                            = '%s отзывов';
$_['text_write']                              = 'Написать отзыв';
$_['text_login']                              = 'Пожалуйста <a href="%s"> войдите</a> или <a href="%s"> зарегистрируйтесь</a> для просмотра';
$_['text_no_reviews']                         = 'Нет отзывов об этом товаре.';
$_['text_note']                               = '<span class="text-danger">Внимание:</span> HTML не переведен!';
$_['text_success']             				  = 'Спасибо за отзыв. Он будет обработан и опубликован.';


// Error
$_['error_name']                              = 'Имя должно быть от 3 до 25 символов!';
$_['error_text']                              = 'Текст Отзыва должен быть от 25 до 1000 символов!';
$_['error_rating']                            = 'Пожалуйста поставьте оценку!';
$_['error_captcha']                           = 'Код подтверждения не соответствует картинке!';
