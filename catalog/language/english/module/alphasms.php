<?php

$_['alphasms_saved_success'] = 'Success saved settings';

$_['alphasms_message_connection_error'] = 'When you send an alert via SMS could not string any Gateway.<br />Sending time: %s<br />Server response: %s';
$_['alphasms_message_customer_new_register'] = 'Congratulations on your successful registration in the store "{shop_name}".';
$_['alphasms_message_customer_new_order'] = 'Thank you for your purchase. Your order number #{order_id}';
$_['alphasms_message_admin_new_order'] = 'A new order is performed #{order_id}';
$_['alphasms_message_admin_new_customer'] = 'The new customer is registered #{customer_id} {firstname} {lastname}';
$_['alphasms_message_admin_new_email'] = 'It is sent a new mail to support from {email}';

$_['alphasms_page_head_title'] = 'AlphaSMS module';

$_['alphasms_message_customer_new_order_status'] = 'Order status #{order_id} is changed on "{new_status_name}".';
