<?php

$_['alphasms_saved_success'] = 'Настройки удачно сохранены';

$_['alphasms_message_connection_error'] = 'При отправке оповещения по SMS возникли неполадки со шлюзом AlphaSms.ua.<br />Время отправки: %s<br />Ответ сервера: %s';
$_['alphasms_message_customer_new_register'] = 'Поздравляем с успешной регистрацией в Интернет-магазине "{shop_name}"';
$_['alphasms_message_customer_new_order'] = 'Спасибо за покупку. Ваш номер заказа #{order_id}';
$_['alphasms_message_admin_new_customer'] = 'Зарегистрирован новый покупатель #{customer_id} {firstname} {lastname}';
$_['alphasms_message_admin_new_order'] = 'Новый заказ #{order_id}';
$_['alphasms_message_admin_new_email'] = 'В сапорт отправлено письмо от {email}';

$_['alphasms_page_head_title'] = 'Модуль AlphaSMS';

$_['alphasms_message_customer_new_order_status'] = 'Статус заказа #{order_id} изменился на "{new_status_name}"';
