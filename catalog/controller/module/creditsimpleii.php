<?php
class ControllerModuleCreditSimpleII extends Controller {

	public function index(){
		 
		$this->language->load('payment/privatbank_paymentparts_ii');

		$data['button_confirm'] = $this->language->get('button_confirm');
        $data['text_label_partsCount'] = $this->language->get('text_label_partsCount');
        $partsCount = $this->config->get('privatbank_paymentparts_ii_paymentquantity');
        $partsCountArr = array();
		foreach ($this->cart->getProducts() as $cart) {
			$privat_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_privat WHERE product_id = '" . (int)$cart['product_id'] . "'");
			if ($privat_query->row) {
				if ($privat_query->row['partscount_ii'] <= $partsCount && $privat_query->row['partscount_ii'] !=0) {
					$partsCount = (int)$privat_query->row['partscount_ii'];
				}
			}
		}
        for($i=$partsCount;$i>=1;$i--){
            $partsCountArr[] = $i;           
        }
        $data['partsCounts'] = $partsCountArr;
		
		
		if (isset ($this->session->data['privatbank_paymentparts_ii_sel'])) {
			$data['partsCountSel'] = $this->session->data['privatbank_paymentparts_ii_sel'];
		}else {
			$data['partsCountSel'] = '';
		}		
        
		if (!$this->config->get('privatbank_paymentparts_ii_test')) {
            $data['action'] = $this->url->link('payment/privatbank_paymentparts_ii/sendDataDeal', '', 'SSL');
		} else {
			$data['action'] = 'https://brtp.test.it.loc/ipp/';
		}

		if(version_compare( VERSION, '2.2.0.0', '>=' )) {
			$this->response->setOutput($this->load->view('module/credit_simple_ii', $data));
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/credit_simple_ii.tpl')) {
				return $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/credit_simple_ii.tpl', $data));
			} else {
				return $this->response->setOutput($this->load->view('default/template/module/credit_simple_ii.tpl', $data));
			}
		}
    }
}