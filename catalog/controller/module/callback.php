<?php
class ControllerModuleCallback extends Controller {
	public function index() {
		$this->load->language('module/callback');
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_holder_name'] = $this->language->get('text_holder_name');
		$data['text_holder_tel'] = $this->language->get('text_holder_tel');
		$data['text_question'] = $this->language->get('text_question');
		$data['button_send'] = $this->language->get('button_send');

		$data['action'] = $this->url->link('module/callback/addRequest');


		$this->load->model('module/callback');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/callback.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/callback.tpl', $data);
		} else {
			return $this->load->view('default/template/module/callback.tpl', $data);
		}

	}

	public function addRequest (){

		$this->load->language('module/callback');

		$json = '';
		$data = array();

		if (isset($_POST['name']) && !empty($_POST['name'])){
			$data['name'] = htmlspecialchars($_POST['name']);
		} else {
			$json['error']['name'] = $this->language->get('error_name');
		}

		if (isset($_POST['tel']) && !empty($_POST['tel'])){
			$data['tel'] = htmlspecialchars($_POST['tel']);
		} else {
			$json['error']['tel'] = $this->language->get('error_phone');
		}

		if (isset($_POST['text']) && !empty($_POST['text'])){
			$data['text'] = htmlspecialchars($_POST['text']);
		}

		if (isset($_POST['link']) && !empty($_POST['link'])){
			$data['link'] = htmlspecialchars($_POST['link']);
		}

		if(!isset($json['error'])){

			$this->load->model('module/callback');

			$this->model_module_callback->addCallback($data);

			$json['success'] = $this->language->get('success');;

		}

		return $this->response->setOutput(json_encode($json));

	}
}