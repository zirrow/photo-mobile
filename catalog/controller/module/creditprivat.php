<?php
class ControllerModuleCreditprivat extends Controller {

	 public function index(){
		 
        if (isset($this->request->get['product_id'])) {
            $product_id = $this->request->get['product_id'];
			$this->load->model('catalog/product');
			$credit_info = $this->model_catalog_product->getProductPrivat($this->request->get['product_id']);
        } else {
            $product_id = 0;
        }
		
//		$thiscart = $this->cart->getProducts();
		

		$data['status_pp'] = false;
		$data['status_ii'] = false;
//		$thiscartlast = end($thiscart);
//		$data['total'] = $thiscartlast['price'];
//		$data['total'] = (int)str_replace(" ","",$this->currency->format($this->tax->calculate($thiscartlast['price'], $thiscartlast['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']));
		$data['total'] = (int)str_replace(" ","",$this->currency->format($this->tax->calculate($this->session->data['private_price'], $this->session->data['private_tax'], $this->config->get('config_tax')), $this->session->data['currency']));
		
		$this->session->data['privatbank_paymentparts_pp_sel_total'] = $data['total'];

		if ($this->config->get('privatbank_paymentparts_pp_status') == 1) {
			
			if ((!$this->config->get('privatbank_paymentparts_pp_product_allowed'))	|| ($this->config->get('privatbank_paymentparts_pp_product_allowed') && in_array($product_id, $this->config->get('privatbank_paymentparts_pp_product_allowed')))) {
			
				if ( ($this->config->get('privatbank_paymentparts_pp_min_total') <= $data['total']) && (($this->config->get('privatbank_paymentparts_pp_max_total')) >= $data['total'])) {

					$data['status_pp'] = true;
				}
			}
		}
		
		if ($this->config->get('privatbank_paymentparts_ii_status') == 1) {
			
			if ((!$this->config->get('privatbank_paymentparts_ii_product_allowed'))	|| ($this->config->get('privatbank_paymentparts_ii_product_allowed') && in_array($product_id, $this->config->get('privatbank_paymentparts_ii_product_allowed')))) {
			
				if ( ($this->config->get('privatbank_paymentparts_ii_min_total') <= $data['total']) && (($this->config->get('privatbank_paymentparts_ii_max_total')) >= $data['total'])) {

					$data['status_ii'] = true;
				}
			}
		}

		$data['partsCountpp'] = (!$this->config->get('privatbank_paymentparts_pp_paymentquantity') ? '24' : $this->config->get('privatbank_paymentparts_pp_paymentquantity'));
		$data['partsCountii'] = (!$this->config->get('privatbank_paymentparts_ii_paymentquantity') ? '24' : $this->config->get('privatbank_paymentparts_ii_paymentquantity'));

		$data['markuppp'] = (!$this->config->get('privatbank_paymentparts_pp_markup') ? '1' : $this->config->get('privatbank_paymentparts_pp_markup'));
		$data['markupii'] = (!$this->config->get('privatbank_paymentparts_ii_markup') ? '1' : $this->config->get('privatbank_paymentparts_ii_markup'));
		
		if ($credit_info) {
			$data['partsCountpp'] = $credit_info['partscount_pp'] !=0 ? $credit_info['partscount_pp'] : $data['partsCountpp'];
			$data['partsCountii'] = $credit_info['partscount_ii'] !=0 ? $credit_info['partscount_ii'] : $data['partsCountii'];
			$data['markuppp'] = $credit_info['markup_pp'] !=0 ? $credit_info['markup_pp'] : $data['markuppp'];
			$data['markupii'] = $credit_info['markup_ii'] !=0 ? $credit_info['markup_ii'] : $data['markupii'];
		}
		
		if(version_compare( VERSION, '2.2.0.0', '>=' )) {
			$this->response->setOutput($this->load->view('module/creditprivat', $data));
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/creditprivat.tpl')) {
				return $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/creditprivat.tpl', $data));
			} else {
				return $this->response->setOutput($this->load->view('default/template/module/creditprivat.tpl', $data));
			}
		}
    }
}