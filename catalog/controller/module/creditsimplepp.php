<?php
class ControllerModuleCreditSimplepp extends Controller {

	public function index(){
		 
		$this->language->load('payment/privatbank_paymentparts_pp');

		$data['button_confirm'] = $this->language->get('button_confirm');
        $data['text_label_partsCount'] = $this->language->get('text_label_partsCount');
        $partsCount = $this->config->get('privatbank_paymentparts_pp_paymentquantity');
		foreach ($this->cart->getProducts() as $cart) {
			$privat_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_privat WHERE product_id = '" . (int)$cart['product_id'] . "'");
			if ($privat_query->row) {
				if ($privat_query->row['partscount_pp'] <= $partsCount && $privat_query->row['partscount_pp'] !=0) {
					$partsCount = (int)$privat_query->row['partscount_pp'];
				}
			}
		}
        $partsCountArr = array();
        for($i=$partsCount;$i>=1;$i--){
            $partsCountArr[] = $i;           
        }
        $data['partsCounts'] = $partsCountArr;
		
		
		if (isset ($this->session->data['privatbank_paymentparts_pp_sel'])) {
			$data['partsCountSel'] = $this->session->data['privatbank_paymentparts_pp_sel'];
		}else {
			$data['partsCountSel'] = '';
		}		
        
		if (!$this->config->get('privatbank_paymentparts_pp_test')) {
            $data['action'] = $this->url->link('payment/privatbank_paymentparts_pp/sendDataDeal', '', 'SSL');
		} else {
			$data['action'] = 'https://brtp.test.it.loc/ipp/';
		}

		if(version_compare( VERSION, '2.2.0.0', '>=' )) {
			$this->response->setOutput($this->load->view('module/credit_simple_pp', $data));
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/credit_simple_pp.tpl')) {
				return $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/credit_simple_pp.tpl', $data));
			} else {
				return $this->response->setOutput($this->load->view('default/template/module/credit_simple_pp.tpl', $data));
			}
		}
    }
}