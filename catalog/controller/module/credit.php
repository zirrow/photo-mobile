<?php
class ControllerModuleCredit extends Controller {
    public function index() {

		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_buycredit'] = $this->language->get('text_buycredit');
		
        if (isset($this->request->get['product_id'])) {
            $product_id = $this->request->get['product_id'];
        } else {
            $product_id = 0;
        }
		
		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		$status = false;

		if ($product_info) {
					
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = (int)str_replace(" ","",$this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']));
			} else {
				$price = false;
			}

			if ((float)$product_info['special']) {
				$special = (int)str_replace(" ","",$this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']));
			} else {
				$special = false;
			}

			if (!$special) {
				$creditprice = $price;
			} else {
				$creditprice = $special;
			}
			
			if ($this->config->get('privatbank_paymentparts_pp_status') == 1) {
				
				if ((!$this->config->get('privatbank_paymentparts_pp_product_allowed'))	|| ($this->config->get('privatbank_paymentparts_pp_product_allowed') && in_array($product_id, $this->config->get('privatbank_paymentparts_pp_product_allowed')))) {
				
					if ( ($this->config->get('privatbank_paymentparts_pp_min_total') <= $creditprice) && (($this->config->get('privatbank_paymentparts_pp_max_total')) >= $creditprice)) {

						$status = true;
					}
				}
			}			
			
			if ($this->config->get('privatbank_paymentparts_ii_status') == 1) {
				
				if ((!$this->config->get('privatbank_paymentparts_ii_product_allowed'))	|| ($this->config->get('privatbank_paymentparts_ii_product_allowed') && in_array($product_id, $this->config->get('privatbank_paymentparts_ii_product_allowed')))) {
				
					if ( ($this->config->get('privatbank_paymentparts_ii_min_total') <= $creditprice) && (($this->config->get('privatbank_paymentparts_ii_max_total')) >= $creditprice)) {

						$status = true;
					}
				}
			}			
		}
		
		if ($status) {
			if(version_compare( VERSION, '2.2.0.0', '>=' )) {
				return $this->load->view('module/credit_button', $data);
			} else {
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/credit_button.tpl')) {
					return $this->load->view($this->config->get('config_template') . '/template/module/credit_button.tpl', $data);
				} else {
					return $this->load->view('default/template/module/credit_button.tpl', $data);
				}
			}
		}
    }
	
    public function loadpopup() {

        if (isset($this->request->get['product_id'])) {
            $data['product_id'] = $this->request->get['product_id'];
        } else {
            $data['product_id'] = 0;
        }

		$this->session->data['privatbank_paymentparts_product_id'] = $data['product_id'];
		
		if(version_compare( VERSION, '2.2.0.0', '>=' )) {
				$this->response->setOutput($this->load->view('module/credit', $data));
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/credit.tpl')) {
				return $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/credit.tpl', $data));
			} else {
				return $this->response->setOutput($this->load->view('default/template/module/credit.tpl', $data));
			}
		}	
    }	
}