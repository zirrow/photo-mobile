<?php
class ControllerFeedGoogleCsv extends Controller {
	public function index() {

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$products = $this->model_catalog_product->getProducts();

		$cat_exclude_ids = [81,116,117,155]; //массив с каталогами

		foreach ($products as $product){

			$category_array = $this->model_catalog_product->getCategories($product['product_id']);

			$minimaze_array = array_map(function($category){ //преобразовываем многомерный массив в одномерный
				return $category['category_id'];
			}, $category_array);

			if(count(array_intersect($minimaze_array,$cat_exclude_ids))){ //если в одном массиве есть элементы другого массива то функция даст положительный результат и цикл перезапустится без добавления данных
				continue;
			}

			if(isset($product['special']) && !empty($product['special'])){
				$price = $product['special'];
			} else {
				$price = $product['price'];
			}

			$product_array[] = array(
				'id' => $product['sku'],
				'name' => $product['name'],
				'model' => $product['model'],
				'image' => HTTPS_SERVER.'image/'.$product['image'],
				'price' => round((float) $price).'.00 UAH',
				'link' => html_entity_decode($this->url->link('product/product','product_id='.$product['product_id'],'SSL'))
			);
		}

		header('Content-Type: text/csv');
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename="google_rem.csv"');

		echo '"ID","Item title","Final Url","Image URL","Price"'."\r\n";
		foreach ($product_array as $fields) {
			echo '"'.$fields['id'].'","'.$fields['name'].'","'.$fields['link'].'","'.$fields['image'].'","'.$fields['price'].'"'."\r\n";
		}

		return true;
	}
}
