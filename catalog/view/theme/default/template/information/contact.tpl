<?php echo $header; ?>
<div class="container">

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php include DIR_APPLICATION . '/view/theme/default/template/_breadcrumb.tpl'; ?>
      <h1><?php echo $heading_title; ?></h1>
      <h3><?php echo $text_location; ?></h3>
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <?php if ($image) { ?>
            <div class="col-sm-3"><img src="<?php echo $image; ?>" alt="<?php echo $store; ?>" title="<?php echo $store; ?>" class="img-thumbnail" /></div>
            <?php } ?>
            <div class="col-sm-3"><strong><?php echo $store; ?></strong><br />
              <address>
              <?php echo $address; ?>
              </address>
              <?php if ($geocode) { ?>
              <a href="https://maps.google.com/maps?q=<?php echo urlencode($geocode); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
              <?php } ?>
            </div>
            <div class="col-sm-3"><strong><?php echo $text_telephone; ?></strong><br>
              <?php $telephone1 = preg_replace("/[a-zA-Zа-яА-Я,(),-]/", '', $telephone); ?>
              <a href="tel:<?php echo $telephone1; ?>"><?php echo $telephone ?></a><br />
              <?php echo html_entity_decode($telephone_add); ?>
              <br />
              <?php if ($fax) { ?>
              <strong><?php echo $text_fax; ?></strong><br>
              <?php echo $fax; ?>
              <?php } ?>
            </div>
            <div class="col-sm-3">
              <?php if ($open) { ?>
              <strong><?php echo $text_open; ?></strong><br />
              <?php echo $open; ?><br />
              <br />
              <?php } ?>
              <?php if ($comment) { ?>
              <strong><?php echo $text_comment; ?></strong><br />
              <?php echo $comment; ?>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>

	    <?php if ($locations) { ?>
	        <h3><?php echo $text_store; ?></h3>
			    <?php foreach ($locations as $location) { ?>
				    <div class="panel panel-default contacts-array">
					    <div class="panel-body">

						    <div class="row locale_item">
							    <div class="col-sm-3 images-magnific-<?php echo $location['location_id']; ?>">
								    <?php if ($location['image']) { ?>
									   <img src="<?php echo $location['image']; ?>" alt="<?php echo $location['name']; ?>" title="<?php echo $location['name']; ?>" class="img-thumbnail images-magnific-<?php echo $location['location_id']; ?>" />
								    <?php } ?>
							    </div>

							    <div class="col-sm-3"><strong><?php echo $location['name']; ?></strong><br />
								    <address>
									    <?php echo $location['address']; ?>
								    </address>

								    <?php if ($location['geocode']) { ?>
								    <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
								    <?php } ?>
							    </div>

							    <div class="col-sm-3">
								    <strong><?php echo $text_telephone; ?></strong><br>
								    <?php echo $location['telephone']; ?><br />
								    <br />

								    <?php if ($location['fax']) { ?>
								    <strong><?php echo $text_fax; ?></strong><br>
								    <?php echo $location['fax']; ?>
								    <?php } ?>

								    <br />
								    <?php if ($location['open']) { ?>
									    <strong><?php echo $text_open; ?></strong><br />
									    <?php echo $location['open']; ?><br />
								    <br />
								    <?php } ?>

								    <?php if ($location['comment']) { ?>
									    <strong><?php echo $text_comment; ?></strong><br />
									    <?php echo $location['comment']; ?>
								    <?php } ?>
							    </div>

							    <div class="col-sm-3">

									<?php if($location['video']){ ?>
									    <div style="position:relative;height:0;padding-bottom:56.21%">
										    <iframe id="ytplayer" type="text/html" width="250" height="250"
										            src="https://www.youtube.com/embed/<?php echo $location['video']; ?>?autoplay=0"
										            frameborder="0"></iframe>
									    </div>
								    <?php } ?>

							    </div>
						    </div>
					    </div>
				    </div>
			    <?php } ?>
	    <?php } ?>

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_contact; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
            <div class="col-sm-10">
              <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
              <?php if ($error_enquiry) { ?>
              <div class="text-danger"><?php echo $error_enquiry; ?></div>
              <?php } ?>
            </div>
          </div>
          <?php echo $captcha; ?>
        </fieldset>
        <div class="buttons">
          <div class="pull-right">
            <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<?php echo $footer; ?>

<script type="text/javascript">
    <?php foreach ($locations as $location) { ?>
	    $('.images-magnific-<?php echo $location['location_id']; ?>').magnificPopup({
	        items: [
                <?php if ($location['image']) { ?>
		            {
		                src: '<?php echo $location['image_popap']; ?>'
		            },
                <?php } ?>
                <?php if ($location['video']) { ?>
		            {
		                src: 'http://www.youtube.com/watch?v=<?php echo $location['video']; ?>',
		                type: 'iframe'
		            }
                <?php } ?>
	        ],
	        gallery: {
	            enabled: true
	        },
	        type: 'image'
	    });
    <?php } ?>

</script>