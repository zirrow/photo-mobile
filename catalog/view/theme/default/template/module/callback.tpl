
<a href="" data-toggle="modal" data-target="#callback-popup"><?php echo $button_send; ?></a>

<div id="callback-popup" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><?php echo $heading_title; ?></h4>
			</div>
			<div class="modal-body">
				<div class="error-box"></div>
				<form action="<?php echo $action; ?>" method="post" class="callback-popup__form callback-head-form" id="callback-head-form">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="<?php echo $text_holder_name; ?>" name="name" id="name">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="<?php echo $text_holder_tel; ?>" name="tel" id="tel">
					</div>
					<div class="form-group"><span><?php echo $text_question; ?></span>
						<textarea class="form-control" placeholder="<?php echo $text_question; ?>" name="text" id="text"></textarea>
					</div>
					<input type="hidden" name="link" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
				</form>
			</div>
			<div class="modal-footer">
				<button type="submit" form="callback-head-form" class="btn btn-primary" id="callback-head-submit"><?php echo $button_send; ?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
			</div>
		</div>

	</div>
</div>