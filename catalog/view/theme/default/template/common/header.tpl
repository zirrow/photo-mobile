<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />


    <?php /* if ($merge_css) { ?>
        <?php if ($inline_css) { ?>
            <style>
                <?php echo $css_content; ?>
            </style>
		<?php } else { ?>
            <link href="<?php echo $cache_style; ?>" rel="stylesheet" />
        <?php } ?>
	<?php } else {  */?>
	    <link href="/catalog/view/theme/default/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
	    <link href="/catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
	    <link href="/catalog/view/theme/default/stylesheet/responsive.css" rel="stylesheet">
	<?php // } ?>

	<?php foreach ($styles as $style) { ?>
		<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>


	<?php if ($maincolor) { ?>
		<style>
		    .main-color,.btn.btn-primary,.buttons .btn.btn-primary,#search button:hover,.mini-cart:hover,#cart-total:after,.product-thumb:hover .button-group.button-cart button,#phone-widget ul li.active{background-color:<?php echo $maincolor; ?>}
		    #content p a,#top a:hover,#search button,#column-left a.list-group-item.active,#column-left a.list-group-item.active:focus,#column-left a.list-group-item:hover,#column-right a.list-group-item.active,#column-right a.list-group-item.active:focus,#column-right a.list-group-item:hover,.product-info .price,.product-thumb .caption .price,.product-thumb-left .price{color:<?php echo $maincolor; ?>}
		    .btn.btn-primary,.buttons .btn.btn-primary,#search button:hover,.mini-cart:hover,#content .border .owl-wrapper-outer{border-color:<?php echo $maincolor; ?>}
		    .box-heading,.product-info .price-old:after,.product-tabs .nav.nav-tabs > li.active > a,#phone-widget ul li a{border-bottom-color:<?php echo $maincolor; ?>}
		</style>
	<?php } ?>


	<!-- GOTO -->
	<?php if ($mainhovercolor) { ?>
		<style>
		    .btn.btn-primary:hover,.buttons .btn.btn-primary:hover,#menu .nav > li > a:focus,.nav > li > a:hover, .nav .open > a,#menu .nav .open > a:focus, .nav .open > a:hover,.product-thumb:hover .button-group.button-cart button:hover{background-color:<?php echo $mainhovercolor; ?>}
		    /*#content p a:hover, a:hover,.product-tabs .nav.nav-tabs > li > a:hover{color:<?php echo $mainhovercolor; ?>}*/
		    .btn.btn-primary:hover,.buttons .btn.btn-primary:hover,.nav > li > a:focus,.nav > li > a:hover{border-color:<?php echo $mainhovercolor; ?>}
		</style>
	<?php } ?>

	<?php if ($custom_css) { ?>
		<style>
		    <?php echo $custom_css; ?>
		</style>
	<?php } ?>

	<?php foreach ($links as $link) { ?>
		<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>

	<?php  if (!$scripts_to_footer) { ?>
	    <script src="/catalog/view/theme/default/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript" ></script>
		<script src="/catalog/view/theme/default/javascript/jquery/jquery.mask.min.js" type="text/javascript" ></script>
		<link href="/catalog/view/javascript/jquery/pp_calculator/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen">
		<script src="/catalog/view/javascript/jquery/pp_calculator/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
	    <script src="/catalog/view/theme/default/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
	    <script src="/catalog/view/theme/default/javascript/common.js" type="text/javascript" ></script>

		<?php foreach ($scripts as $script) { ?>
		    <script src="<?php echo $script; ?>" type="text/javascript" ></script>
		<?php } ?>

        <script src="/catalog/view/theme/default/javascript/jquery.validate.js" type="text/javascript" ></script>
	<?php } ?>

	<!-- Facebook Pixel Code -->
		<script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
					n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
					n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
					t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '150795318736795');
            fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=150795318736795&ev=PageView&noscript=1"/></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter48554030 = new Ya.Metrika({
                        id:48554030,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://d31j93rd8oukbv.cloudfront.net/metrika/watch_ua.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/48554030" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

	<script>
	    var domReady = function(callback) {
	        document.readyState === "interactive" || document.readyState === "complete" ? callback() : document.addEventListener("DOMContentLoaded", callback);
	    };
	</script>

	<?php foreach ($analytics as $analytic) { ?>
		<?php echo $analytic; ?>
	<?php } ?>

	<script type="text/javascript">
	  (function(d, w, s) {
	  var widgetHash = 'gngtwpd6ucvcji4u7gjx', ctw = d.createElement(s); ctw.type = 'text/javascript'; ctw.async = true;
	  ctw.src = '//widgets.binotel.com/calltracking/widgets/'+ widgetHash +'.js';
	  var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(ctw, sn);
	  })(document, window, 'script');
	</script>

	<script type="text/javascript">
	  (function(d, w, s) {
	  var widgetHash = 'jbpb9a1rdujezw1rr2tk', gcw = d.createElement(s); gcw.type = 'text/javascript'; gcw.async = true;
	  gcw.src = '//widgets.binotel.com/getcall/widgets/'+ widgetHash +'.js';
	  var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(gcw, sn);
	  })(document, window, 'script');
	</script>
</head>
<body class="<?php echo $class; ?>">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-000000" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<h1 style="display: none"><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></h1>
  <div id="wrapper">
<nav id="top" class="hidden-xs">
  <div class="container">

	  <div class="html-clock-left-head">
		  <?php if ($topleft_html_module) { ?>
		    <?php echo $topleft_html_module; ?>
		  <?php } ?>
	  </div>

        <?php echo $currency; ?>
        <?php echo $language; ?>

      <?php if ((isset($information_categories))&&($information_categories)&&($top_info)) { ?>
	      <div class="pull-left">
		      <div class="btn-group">
		          <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
		            <span><i class="fa fa-book"></i>
		            <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_info; ?></span> <span class="caret"></span></span>
		          </button>
		          <ul class="dropdown-menu">
		            <?php foreach ($information_categories as $category) { ?>
		                <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
		            <?php } ?>
		          </ul>
		      </div>
	      </div>
      <?php } ?>

	    <div id="top-links" class="nav pull-right">
	      <ul class="list-inline">
	          <?php echo $top_html_module; ?>
	          <li><a href="/compare-products/">Сравнение товаров</a></li>
	            <?php if ($logged) { ?>
	          <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
	              <ul class="dropdown-menu dropdown-menu-right top-meny-castom-stiles">
	            <li><a href="<?php echo $account; ?>"><i class="fa fa-user"></i> <?php echo $text_account; ?></a></li>
	            <li><a href="<?php echo $order; ?>"><i class="fa fa-time"></i> <?php echo $text_order; ?></a></li>
	                <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart-o"></i> <?php echo $text_wishlist; ?></a></li>
	                <li><a href="<?php echo $logout; ?>"><i class="fa fa-chevron-left"></i> <?php echo $text_logout; ?></a></li>
	            <?php } else { ?>
	            <?php /*
	                <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
	            */ ?>
	            <li><a href="<?php echo $login; ?>"><i class="fa fa-user"></i> Войти<?php // echo $text_login; ?></a></li>
	            <?php } ?>
	        </li>
	        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
	      </ul>
	    </div>
  </div>
</nav>

	<header class="hidden-xs">
	  <div class="container">
	    <div class="row">
	      <div class="col-20 logo">
	        <div id="logo">
	          <?php if ($logo) { ?>
	            <?php if ($home == $og_url) { ?>
	              <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
	            <?php } else { ?>
	              <a href="<?php echo $home; ?>">
		              <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
	              </a>
	            <?php } ?>
	          <?php } else { ?>
	            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
	          <?php } ?>
	        </div>
	      </div>
	      <div class="col-20 phones">
	         <ul>
	            <li>
		            <a href="tel://0688221919">
			            <img src="image/catalog/viber2.png" width="30" height="25">
			            (068) 822-19-19
		            </a>
	            </li>
				<li>
					<a href="tel:0507762192">
						<i class="p-sprite p-sprite-Voda-Logo"></i>
						(050) 776-21-92
					</a>
				</li>
				<li>Бесплатная доставка</li>
			</ul>
	      </div>
	      <div class="col-20 phones">
	         <ul>
	            <li>
		            <a href="tel:0800210188">
			            <img src="image/catalog/tel.jpg" width="30" height="25">
			            0(800) 210-188
		            </a>
	            </li>
				<li>
					<a href="tel:0731207060">
						<i class="p-sprite p-sprite-Life-Logo"></i>
						(073) 120-70-60</a>
				</li>
				<li>Без предоплат</li>
			</ul>
	      </div>
	      <div class="col-20 rezhim">
	        <ul>
	            <li>9:00 - 18:00</li>
	            <li>без выходных</li>
		        <li><?php echo $callback ?></li>
	            <li>14 дней на возврат</li>
	        </ul>
	      </div>
	      <div class="col-20">
		      <?php echo $cart; ?>
		      <?php echo $search; ?>
	      </div>
	    </div>
	  </div>
	</header>

	<?php echo $cart_popup; ?>

	<?php if ($categories) { ?>
	<div class="navigation main-color" id="main-nav">
		<div class="container">
		  <nav id="menu" class="navbar yamm">
		    <div class="navbar-header">
		      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex11-collapse">
			      <i class="fa fa-bars"></i>
			      <!--<span id="category" class="visible-xs"><?php echo $text_category; ?></span> -->
		      </button>


			    <div class="pull-left hidden visible-xs phone-little-head">
				    <?php if ($logo) { ?>
					    <?php if ($home == $og_url) { ?>
					        <img src="/catalog/view/theme/default/image/logo-m.png" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="logo-little" />
					    <?php } else { ?>
						    <a href="<?php echo $home; ?>">
							    <img src="/catalog/view/theme/default/image/logo-m.png" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="logo-little" />
						    </a>
					    <?php } ?>
				    <?php } else { ?>
				        <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
				    <?php } ?>
			    </div>

		        <div class="pull-right hidden visible-xs">

		            <a href="<?php echo $search_link; ?>" class="btn btn-navbar">
			            <i class="fa fa-search"></i>
		            </a>
			        <br>
			        <a href="<?php echo $checkout; ?>" class="btn btn-navbar">
				        <i class="fa fa-shopping-cart"></i>
			        </a>

		        </div>

			    <div class="pull-right hidden visible-xs phone-little-head phone-little-head-right">
				    <a data-target="#phones-modal" data-toggle="modal" onclick="ga('send', 'event', { eventCategory: 'click-on-phones', eventAction: 'button'});">
					    <img src="image/catalog/viber2.png" width="30" height="25">
					    (068) 822-19-19
				    </a>
				    <br>
				    <a data-target="#phones-modal" data-toggle="modal" onclick="ga('send', 'event', { eventCategory: 'click-on-phones', eventAction: 'button'});">
					    <i class="p-sprite p-sprite-Voda-Logo"></i>
					    (050) 776-21-92
				    </a>
			    </div>

			    <div id="phones-modal" class="modal fade" role="dialog">
				    <div class="modal-dialog modal-sm">
					    <!-- Modal content-->
					    <div class="modal-content">
						    <div class="modal-header">
							    <button type="button" class="close" data-dismiss="modal">&times;</button>
							    <span class="h4 modal-title"><?php echo $text_popup_cart; ?></span>
						    </div>
						    <div class="modal-body modal-phones-list">
							    <ul>
								    <li>
									    <a href="tel://0688221919">
										    <img src="image/catalog/viber2.png" width="30" height="25">
										    (068) 822-19-19
									    </a>
								    </li>
								    <li>
									    <a href="tel:0507762192">
										    <i class="p-sprite p-sprite-Voda-Logo"></i>
										    (050) 776-21-92
									    </a>
								    </li>
								    <li>
									    <a href="tel:0800210188">
										    <img src="image/catalog/tel.jpg" width="30" height="25">
										    0(800) 210-188
									    </a>
								    </li>
								    <li>
									    <a href="tel:0731207060">
										    <i class="p-sprite p-sprite-Life-Logo"></i>
										    (073) 120-70-60</a>
								    </li>
							    </ul>
						    </div>
					    </div>
				    </div>
			    </div>

		    </div>

		    <div class="collapse navbar-collapse navbar-ex11-collapse">
		      <ul class="nav navbar-nav">
		        <?php echo $premenu_html_module; ?>
			        <?php foreach ($categories as $category) { ?>
				        <?php if ($category['children']) { ?>
					        <li class="dropdown">
						        <a href="<?php echo $category['href']; ?>">
							        <div class="like-a"></div>
							        <!-- <img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" class="hidden visible-xs menu-main-image"/> -->
							        <span><?php echo $category['name']; ?></span>
						        </a>

					          <span class="menu-item-toggle close visible-xs">
					            <i class="fa fa-plus"></i>
					            <i class="fa fa-minus"></i>
					          </span>

					          <div class="dropdown-menu ">
					            <div class="dropdown-inner">
					              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
						              <ul class="list-unstyled">
						                <?php foreach ($children as $child) { ?>

						                    <?php if(isset($child['children'])) { // added ?>
						                        <?php // echo nthLevelMenu($child,$text_all); ?>
						                        <li>
						                            <a href="<?php echo $child['href']; ?>">
						                                <!-- <img src="<?php echo $child['image']; ?>" alt="<?php echo $child['name']; ?>" class="hidden visible-xs" style="height: 30px"/> -->
						                                <?php echo $child['name']; ?>
						                            </a>
						                        </li>
						                    <?php } else { ?>
						                        <li>
						                            <a href="<?php echo $child['href']; ?>">
							                            <!-- <img src="<?php echo $child['image']; ?>" alt="<?php echo $child['name']; ?>" class="hidden visible-xs" style="height: 30px"/> -->
						                                <?php echo $child['name']; ?>
						                            </a>
						                        </li>
						                    <?php } //added ?>
						                <?php } ?>
						              </ul>
					              <?php } ?>
					            </div>
					          </div>
					        </li>
				        <?php } else { ?>
				            <li>
					            <a href="<?php echo $category['href']; ?>">
						          <!-- <img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" class="hidden visible-xs menu-main-image"/> -->
						            <span><?php echo $category['name']; ?></span>
					            </a>
				            </li>
				        <?php } ?>
			        <?php } ?>
			      <li class="hidden visible-xs delitel delitel-first">
				      <a href="/dostavka-i-oplata">Доставка и оплата</a>
			      </li>
			      <li class="hidden visible-xs delitel">
				      <a href="/about-us">О нас</a>
			      </li>
			      <li class="hidden visible-xs delitel">
				      <a href="/contact-us/">Контакты</a>
			      </li>
		          <?php echo $aftermenu_html_module; ?>
		      </ul>
		    </div>
		  </nav>
		</div>
	</div>
<?php } ?>
