	<footer>
	  <div class="container">
	    <div class="row">
	        <?php if ($footerone_html_module) { ?>
	            <?php echo $footerone_html_module; ?>
	        <?php } else { ?>
	          <div class="col-sm-3">
	            <div class="footer-heading"><?php echo $text_information; ?></div>
	            <ul class="list-unstyled">
	                <?php if ((isset($information_categories))&&($information_categories)&&(count($information_categories) > 1)) { ?>
	                    <?php foreach ($information_categories as $category) { ?>
	                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
	                    <?php } ?>
	                <?php } ?>
	                <?php if ($informations) { ?>
	                    <?php foreach ($informations as $information) { ?>
	                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
	                    <?php } ?>
	                <?php } ?>
	            </ul>
	          </div>
	        <?php } ?>

	        <?php if ($footertwo_html_module) { ?>
	            <?php echo $footertwo_html_module; ?>
	        <?php } else { ?>
		      <div class="col-sm-3">
		        <div class="footer-heading"><?php echo $text_extra; ?></div>
		        <ul class="list-unstyled">
		          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
		          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
		            <?php /*
		          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
		          */ ?>
		          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
		        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
		        <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
		        </ul>
		      </div>
	        <?php } ?>

	        <?php if ($footerthree_html_module) { ?>
	            <?php echo $footerthree_html_module; ?>
	        <?php } else { ?>
		      <div class="col-sm-3 hidden-xs">
		        <div class="footer-heading"><?php echo $text_account; ?></div>
		        <ul class="list-unstyled">
		          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
		          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
		          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
		          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
		        </ul>
		      </div>
	        <?php } ?>

			<div class="hidden-xs">
		        <?php if ($footerfour_html_module) { ?>
		            <?php echo $footerfour_html_module; ?>
		        <?php } else { ?>
		        <div class="col-sm-3">
		            <div class="footer-heading"><?php echo $text_contact; ?></div>
		            <p class="phones">
		                <?php $telephone1 = preg_replace("/[a-zA-Zа-яА-Я,(),-]/", '', $telephone); ?>
		                <a href="tel:<?php echo $telephone1; ?>"><?php echo $telephone ?> </a><br>
		                <?php echo html_entity_decode($telephone_add); ?>
		            </p>
		            <p>
		                <?php echo $address; ?>
		            </p>
		        </div>
		        <?php } ?>
			</div>
	    </div>

	        <hr>
	    <div class="row">
	      <div class="col-sm-6">
	          <?php if ($povered_html_module) { ?>
	              <?php echo $povered_html_module; ?>
	          <?php } else { ?>
	                <p><?php echo $powered; ?></p>
	          <?php } ?>
	      </div>
	      <div class="col-sm-6">
	          <?php if ($footer_banners) { ?>
	              <noindex><div class="footer-icons">
	                <?php foreach ($footer_banners as $banner) { ?>
	                    <?php if ($banner['link']) { ?>
	                        <a rel=nofollow href="<?php echo $banner['link']; ?>" target="_blank"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
	                    <?php } else { ?>
	                        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
	                    <?php } ?>

	                  <?php } ?>
	                </div>
	              </noindex>
	          <?php } ?>
	      </div>
	    </div>
	</div>

	    <?php  if ($scripts_to_footer) { ?>
		    <script src="/catalog/view/theme/default/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript" ></script>
			<script src="/catalog/view/theme/default/javascript/jquery/jquery.mask.min.js" type="text/javascript" ></script>
			<link href="/catalog/view/javascript/jquery/pp_calculator/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen">
			<script src="/catalog/view/javascript/jquery/pp_calculator/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
		    <script src="/catalog/view/theme/default/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
		    <script src="/catalog/view/theme/default/javascript/common.js" type="text/javascript" ></script>
		    <script src="catalog/view/theme/default/javascript/callback.js" type="text/javascript" ></script>


		    <?php foreach ($scripts as $script) { ?>
		        <script src="<?php echo $script; ?>" type="text/javascript" ></script>
		    <?php } ?>

		    <script src="/catalog/view/theme/default/javascript/jquery.validate.js" type="text/javascript" ></script>
		<?php } ?>
	</footer>
 
</div>
	<?php  if ($show_scrollup) { ?>
		<!--scroll to top-->
		<script>
		    domReady(function(){
		        $('body').append('<div id="toTop" class="btn btn-primary"><i class="fa fa-arrow-up"></i></div>');
		        $(window).scroll(function () {
		            if ($(this).scrollTop() > 100) {
		                $('#toTop').fadeIn();
		            } else {
		                $('#toTop').fadeOut();
		            }
		        });
		        $('#toTop').click(function(){
		            $("html, body").animate({ scrollTop: 0 }, 600);
		            return false;
		        });
		    });
		</script>
		<!--scroll to top-->
	<?php } ?>
	﻿
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-80337585-5"></script>
	<script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-‎80337585-5');
	</script>

	﻿<!-- BEGIN PLERDY CODE -->
	<script type="text/javascript" defer>
        var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
        var _site_hash_code = "1ca01bdef87b731a15e9a2afc10b211f";
        var _suid = 1163;
	</script>
	<script type="text/javascript" defer src="https://a.plerdy.com/public/js/click/main.js"></script>
	<!-- END PLERDY CODE -->

	<script type="text/javascript">
        (function(d, w, s) {
            var widgetHash = 'gngtwpd6ucvcji4u7gjx', ctw = d.createElement(s); ctw.type = 'text/javascript'; ctw.async = true;
            ctw.src = '//widgets.binotel.com/calltracking/widgets/'+ widgetHash +'.js';
            var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(ctw, sn);
        })(document, window, 'script');
	</script>

	<script type="text/javascript">
        (function(d, w, s) {
            var widgetHash = 'jbpb9a1rdujezw1rr2tk', gcw = d.createElement(s); gcw.type = 'text/javascript'; gcw.async = true;
            gcw.src = '//widgets.binotel.com/getcall/widgets/'+ widgetHash +'.js';
            var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(gcw, sn);
        })(document, window, 'script');
	</script>

	﻿<!-- Start SiteHeart code -->
	<script>
        (function(){
            var widget_id = 897434;
            _shcp =[{widget_id : widget_id}];
            var lang =(navigator.language || navigator.systemLanguage
                || navigator.userLanguage ||"en")
                .substr(0,2).toLowerCase();
            var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";
            var hcc = document.createElement("script");
            hcc.type ="text/javascript";
            hcc.async =true;
            hcc.src =("https:"== document.location.protocol ?"https":"http")
                +"://"+ url;
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hcc, s.nextSibling);
        })();
	</script>
	<!-- End SiteHeart code -->

</body>
</html>