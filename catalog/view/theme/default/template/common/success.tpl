<?php echo $header; ?>
	<script>
		if(typeof dataLayer != 'undefined'){
	        dataLayer.push({
	            'event': 'rem',
	            'dynx_itemid': [''],
	            'dynx_pagetype': 'conversion',
	            'dynx_totalvalue': ['']
	        });
		}
	</script>

	<?php if(isset($order_id)){ ?>
		<script>
		    fbq('track', 'Lead', {
		        content_name: 'Заказ <?php echo $order_id; ?>',
		    });
		</script>
	<?php } ?>

	<div class="container">

	  <div class="row"><?php echo $column_left; ?>

	    <?php if ($column_left && $column_right) { ?>
	        <?php $class = 'col-sm-6'; ?>
	    <?php } elseif ($column_left || $column_right) { ?>
	        <?php $class = 'col-sm-9'; ?>
	    <?php } else { ?>
	        <?php $class = 'col-sm-12'; ?>
	    <?php } ?>

	    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

	      <?php include DIR_APPLICATION . '/view/theme/default/template/_breadcrumb.tpl'; ?>

	      <div class="allright">
		      <img src="//photo-mobile.com.ua/image/zakaz-devaisa-vipolnen.png" alt="заказ успешно выполнен">
	      </div>

	      <h1><?php echo $text_h1; ?></h1>
	      <div style="text-align:center"><?php echo $text_message; ?></div>

		    <div class="buttons">
	        <div class="pull-right" style="text-align:center">
		        <a href="<?php echo $continue; ?>" class="btn btn-primary">
			        <?php echo $button_continue; ?>
		        </a>
	        </div>
	      </div>
	      <?php echo $content_bottom; ?>
	    </div>
	    <?php echo $column_right; ?>
	  </div>
	</div>
<?php echo $footer; ?>