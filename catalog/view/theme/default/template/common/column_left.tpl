<?php if ($modules) { ?>
<a class="btn btn-lg btn-primary visible-xs" id="show-menu">Уточнить поиск</a>
<aside id="column-left" class="col-sm-3 hidden-xs">
    <?php foreach ($modules as $module) { ?>
    <?php echo $module; ?>
    <?php } ?>
</aside>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function (event) {
        $('#show-menu').click(function () {
            $('#column-left').toggleClass('hidden-xs');
            $('#product-controls').toggleClass('hidden-xs');
        })
    });
</script>
<?php } ?>
