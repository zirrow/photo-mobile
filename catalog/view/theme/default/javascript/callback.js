(function ($) {
    $(document).ready(function(){
        $('#callback-head-form').on('submit', function(event){
            event.preventDefault();

            $.ajax({
                type: 'POST',
                url: 'index.php?route=module/callback/addRequest',
                data: $("#callback-head-form").serialize(),
                dataType: 'json',
                success: function(data) {
                    if (data['error']) {
                        if (data['error']['name']) {
                            $('#callback-head-form input[name=\'name\']').addClass('error');
                            $('#callback-head-form input[name=\'name\']').after('<label id=\'name-error\' class=\'error\' for=\'name\'>'+data['error']['name']+'</label>');
                        }
                        if (data['error']['tel']) {
                            $('#callback-head-form input[name=\'tel\']').addClass('error');
                            $('#callback-head-form input[name=\'tel\']').after('<label id=\'phone-error\' class=\'error\' for=\'tel\'>'+data['error']['tel']+'</label>');
                        }
                    } else {
                        $('#callback-head-form').hide();
                        $('.error-box').html(data['success']).css('color','#66ca3b');
                    }

                }
            });
        });
    });
})(jQuery);
