<?php
class ModelPaymentPrivatbankPaymentpartsPp extends Model {
	public function getMethod($address, $total) {
		$this->load->language('payment/privatbank_paymentparts_pp');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('privatbank_paymentparts_pp_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if ($this->config->get('privatbank_paymentparts_pp_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('privatbank_paymentparts_pp_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		if ($this->config->get('privatbank_paymentparts_pp_product_allowed')) {
			$products = $this->cart->getProducts();
			foreach ($products as $product) {
				if (!in_array($product['product_id'], $this->config->get('privatbank_paymentparts_pp_product_allowed'))) {
					$status = false;
				 }
			}	
		}
		
		if ($this->config->get('privatbank_paymentparts_pp_min_total') > $total) {
			$status = false;
		}
		if ($this->config->get('privatbank_paymentparts_pp_max_total') < $total) {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$method_data = array(
				'code'       => 'privatbank_paymentparts_pp',
				'title'      => $this->language->get('text_title'),
				'title1'      => $this->language->get('text_title1'),
				'terms'      => '',
				'sort_order' => $this->config->get('privatbank_paymentparts_pp_sort_order')
			);
		}
        
		return $method_data;
	}
}
